# -*- coding: utf-8 -*-
"""
Created on Wed May  2 10:47:52 2018

@author: SoumyadeepB

NOTE: to run the DOcker service commands

docker service create --mount type=bind,source=/home/ubuntu/ast_scripts/transferlearning,destination=/home/transferlearning --restart-condition=on-failure --name transferlearning -w /home/transferlearning --replicas=1 --env repl=1 --env t="{{.Task.Slot}}" shambakey1/vifi_astronomy python CRTS_SSS_TransferLearningBy_Finetuning.py
"""


# ALL IMPORTS MUST GO IN THE DOCKER IMAGE
from sklearn.metrics import confusion_matrix, roc_curve, auc,accuracy_score
import shelve, time
import os
from random import random
import numpy as np
from numpy import array
from numpy import cumsum
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import TimeDistributed
import csv
from random import shuffle
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, roc_curve, auc,accuracy_score

from keras.layers import LSTM, Dropout
from keras import optimizers
from itertools import cycle
import itertools
from scipy.stats import kurtosis
from scipy.stats import skew
from astropy.stats import median_absolute_deviation
# create a sequence classification instance

def get_sequence(n_timesteps):
	# create a sequence of random numbers in [0,1]
	X = array([random() for _ in range(n_timesteps)])
	# calculate cut-off value to change class values
	limit = n_timesteps/4.0
	# determine the class outcome for each item in cumulative sequence
	y = array([0 if x < limit else 1 for x in cumsum(X)])
	# reshape input and output data to be suitable for LSTMs
	X = X.reshape(1, n_timesteps, 1)
	y = y.reshape(1, n_timesteps, 1)
	return X, y

def plot_confusion_matrix(test_y, pred_y, class_names, filename):
    """
    This function prints and plots the confusion matrix.
    """
    cmap = plt.cm.Blues
    # Compute confusion matrix
    cm = confusion_matrix(
        np.argmax(test_y, axis=1), np.argmax(pred_y, axis=1))
    np.set_printoptions(precision=2)
    # Plot confusion matrix
    plt.figure()
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title("LSTM Confusion Matrix")
    plt.colorbar()
    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names, rotation=45)
    plt.yticks(tick_marks, class_names)
    print(cm)
    thresh = cm.max() / 2.
    print(thresh)
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig(filename + ".png")
    return cm


def plotroc(test_y, pred_y, n_classes, filename):
    """
    Compute ROC curve and ROC area for each class
    """
    fpr = dict()
    tpr = dict()
    threshold = dict()
    roc_auc = dict()
    for i in range(n_classes):
        fpr[i], tpr[i], threshold[i] = roc_curve(test_y[:, i], pred_y[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
        
    # Plot all ROC curves
    plt.figure()
    lw = 1
    colors = cycle(['aqua', 'darkorange', 'cornflowerblue','g', 'r', 'c', 'm', 'y'])
    for i, color in zip(range(n_classes), colors):
        plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                 label='ROC curve of class {0} (area = {1:0.2f})'''.
                 format(i, roc_auc[i]))
    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
    plt.xlim([0.0, 1])
    plt.ylim([0.0, 1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('LSTM Performance')
    plt.legend(loc="lower right")
    plt.savefig(filename + '.png')
    return


    # Plot normalized confusion matrix
def convert_labelformat(y_m):
    y_label=np.zeros(len(y_m))
    for l in range(0,len(y_m)):
        if(len(np.where(y_m[l]>0)[0])>0):
            y_label[l]=np.where(y_m[l]>0)[0][0]
    return y_label

def StackedLSTMModel(num_var,win_size):
    model = Sequential()
    model.add(LSTM(128, input_dim=num_var, input_length=win_size, return_sequences=True))
    model.add(Dropout(0.2))
#    model.add(LSTM(128, return_sequences=True))
#    model.add(Dropout(0.2))
    model.add(LSTM(128, return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(8, activation='softmax'))
    return model
    
def LearnStackedLSTM(num_var,win_size):
    model = Sequential()
    model.add(LSTM(128, input_dim=num_var, input_length=win_size, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(128, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(128, return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(5, activation='softmax'))
    opt = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999,
                          epsilon=1e-08, decay=0.0)
    model.compile(optimizer='adam', loss='categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()

    # Fit the network
    return (model)

def LearnConvLSTM(num_var,win_size):
    model = Sequential()
    model.add(Conv1D(filters=8, input_dim=num_var, input_length=win_size, kernel_size=3, padding='same', activation='relu'))
    model.add(MaxPooling1D(pool_size=2))
    model.add(LSTM(128, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(128, return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(128, return_sequences=False))
    model.add(Dropout(0.2))
#    model.add(Dense(128, activation='relu'))
#    model.add(Dropout(0.2))
    model.add(Dense(5, activation='softmax'))
    opt = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999,
                          epsilon=1e-08, decay=0.0)
    model.compile(optimizer='adam', loss='categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    
    # Fit the network
    return (model)

def ComputeROC(test_y, pred_y, n_classes):
    """
    Compute ROC curve and ROC area for each class
    """
    fpr = dict()
    tpr = dict()
    threshold = dict()
    roc_auc = dict()
    for i in range(n_classes):
        fpr[i], tpr[i], threshold[i] = roc_curve(test_y[:, i], pred_y[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    return roc_auc,tpr,fpr

def Compute_FMeasure(result,Y_TsAll):
    tp=0
    fp=0
    tn=0
    fn=0
    for l in range(0,len(Y_TsAll)):
        if(Y_TsAll[l]==0):
            if(result[l]==0):
                tp=tp+1
            if(result[l]==1):
                fn=fn+1
        if(Y_TsAll[l]==1):
            if(result[l]==1):
                tn=tn+1
            if(result[l]==0):
                fp=fp+1  
    prec=tp/float(tp+fp)
    rec=tp/float(tp+fn)
    FMeasure=(2*prec*rec)/float(prec+rec) 
    TPR=tp/float(tp+fn)
    FPR=fp/float(fp+tn)
    return FMeasure,prec,rec,TPR,FPR 
